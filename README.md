# HomeVision Take Home Project

## Configuration

There is a config.json in the src folder.  In there you can set the following properties:

- **maxRetries** - How many times should we retry the connection if the server fails?  Setting this to 1 will cause a good amount of failures.
- **itemsPerPage** - How many items per page to load.  This is passed directly to the api call as a query param
- **baseUrl** - The server to hit.

## Running the server

This was started with create-react-app so just run the following command to start the server and have it open for you.

### `yarn start`

Runs the app.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


