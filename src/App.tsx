import React, { useEffect, useReducer, useState, useCallback } from 'react';
import { Container, Box, Button, List, ListItem } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import Config from './config.json';

interface House {
  address: string;
  homeowner: string;
  id: number;
  photoURL: string;
  price: number;
}

// We want to start on page 1, the infinite scroll triggers the "next" call on 
//   mount so we start at 0 so the first request sent is 1
const initialPage = 0;

// This is written abstract like this in case we wanted to add more actions ot
//   this reducer later
interface nextPageActionInterface {
  type: 'NEXT_PAGE';
}

const nextPageAction = (): nextPageActionInterface => ({
  type: NEXT_PAGE
});

const NEXT_PAGE = 'NEXT_PAGE';

// Use reducer to keep track of what page we are on.  Infinite scroll dispatches
//  next page call if we aren't currently loading a call already or have failed
//  too many times.
function pagingReducer(state: number, action: nextPageActionInterface) {
  if(action.type === NEXT_PAGE) {
    state = state + 1;
  }

  return state;
}

// Custom hook to make the request and keep the data.  Use Generics to make
//   it reusable for any list of data.
function useFetch<T> (url: string): [T[], boolean, boolean, () => void] {
  // List of all results, gets appended to as infinite scroll loads more
  const [data, setData] = useState<T[]>([]);

  // Once we've hit the max number of retries this gets set to true
  const [error, setError] = useState(false);

  // A loading boolean so infinite scroll request more than 1 page at time.
  const [loading, setLoading] = useState(false);

  // A boolean to flip to force a re-fetch.  `retryTrigger` is passed in as 
  //   a dependency to useEffect so when it changes it forces useEffect to run
  const [retryTrigger, setRetryTrigger] = useState(false);

  // Memoized function with no dependencies so when this function is passed
  //   as a dependency to the useEffect callback it doesn't cause an infinite
  //   loop of requests.
  const addData = useCallback((newData: T[]): void => {
    setData(previousData => [...previousData, ...newData]);
  }, []);

  // useEffect lets us control when the fetch is called via the dependency
  //   array, whenever the url, or retryTrigger are changed it triggers
  //   this effect to run eventually on success call addData.
  useEffect(() => {
    // Keep retries local in this effect.  Without this we would have to pass
    //   them in as a dependency and bumping or resetting retries would trigger
    //   a fetch.  We don't want that, we need more control so keep it local.
    let retries = 0;
    
    // This function bumps the retries and also decides if we've reached the max.
    //   If we have reached the max it sets the error to true and loading to
    //   false.  If we haven't reached the max then it bumps the retry counter
    //   and calls fetchData again.
    const retryFetch = () => {
      const newRetries = retries + 1;

        if(newRetries >= Config.maxRetries) {
          setError(true);
          setLoading(false);
        } else {
          retries = newRetries;
          fetchData();
        }
    };

    // The actual data fetching happens here.  It checks if the call was a 
    //   failure.  If it was then it calls retryFetch, otherwise it adds
    //   the data via `addData` and sets loading to false.
    const fetchData = async () => {
      console.count('called fetchData');
      try {
        setLoading(true);
        const resp = await fetch(`${Config.baseUrl}${url}`);

        if(!resp.ok) {
          return retryFetch();
        }

        const json = await resp.json();

        addData(json.houses);
        setLoading(false);
      } catch(e) {
        console.log(e);
        return retryFetch();
      }
    }

    // Initial fetch data call to kickoff the sequence whenever the dependencies
    //   change.
    fetchData();
  }, [url, addData, retryTrigger]);

  // Once we have reached the max number of failures this function provides a 
  //   way to basically reset the retry counter by toggling the retry trigger.
  //   when it is toggled it forces a dependency change in the useEffect above
  //   which will start a new attempt of getting the page.
  const retryFetch = useCallback(() => {
    setError(false);
    setRetryTrigger(previousValue => !previousValue);
  }, []);

  return [data, loading, error, retryFetch];
}

function App() {
  // Page is passed in via url so keep it here along with dispatch which is called in infinite scroll.
  const [page, dispatch] = useReducer(pagingReducer, initialPage);

  // Get initial data, also pass in House interface 
  const [data, loading, error, retryFetch] = useFetch<House>(`/api_project/houses?page=${page}&per_page=${Config.itemsPerPage}`);

  // Use 3rd party plugin for infinite scroll.
  const infiniteScrollRef = useInfiniteScroll<HTMLDivElement>({
    loading,
    hasNextPage: true,
    onLoadMore: () => {
      if(!loading && !error) { 
        dispatch(nextPageAction());
      }
    }
  });

  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  

  return (
    <Container>
      <Box m="2rem">
        <List component="div" ref={infiniteScrollRef}>
          {data.map(item => (
            <ListItem>{item.id} - {item.address} - {item.homeowner} - {formatter.format(item.price)}</ListItem>
          ))}
        </List>

        {/* On max retries reached show button to reset retries and try again */ }
        {error && (
          <Alert severity="error">
            <Button variant="contained" color="primary" onClick={() => retryFetch()}>Retry Fetch</Button>
          </Alert>
        )}
      </Box>
    </Container>
  );
}

export default App;
